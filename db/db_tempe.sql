-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2021 at 07:06 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tempe`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `EMAIL` varchar(50) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`EMAIL`, `PASSWORD`) VALUES
('admin@admin.com', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akun`
--

CREATE TABLE `tbl_akun` (
  `EMAIL` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `NAMA` varchar(50) NOT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `JENIS_KELAMIN` varchar(15) NOT NULL,
  `ALAMAT` text NOT NULL,
  `KODE_KOTA` int(5) NOT NULL,
  `TELEPON` varchar(15) NOT NULL,
  `TANGGAL_DAFTAR` datetime DEFAULT current_timestamp(),
  `FOTO` varchar(200) NOT NULL DEFAULT 'picture-1608781686988.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akun`
--

INSERT INTO `tbl_akun` (`EMAIL`, `PASSWORD`, `NAMA`, `TANGGAL_LAHIR`, `JENIS_KELAMIN`, `ALAMAT`, `KODE_KOTA`, `TELEPON`, `TANGGAL_DAFTAR`, `FOTO`) VALUES
('iqbal@tes.com', 'iqbal123', 'Iqbal', '2020-12-24', 'Laki-laki', 'Kencanawangi', 23, '081223002824', '2020-12-25 15:05:57', 'picture-1608781686988.jpg'),
('iqbaldwi15@gmail.com', '081220700605', 'Ardiansyah', '1996-07-08', 'Laki-laki', 'Jl. Kencanawangi XII Blok.P No.19', 23, '081223002824', '2020-12-25 15:04:50', 'picture-1608883490321.jpg'),
('tes@tes.com', 'tes123', 'Iqbal Dwi Ardiansyah', '1996-07-08', 'Laki-laki', 'Jl.kencanawangi XII Blok.P No.19', 23, '081220700605', '2020-06-10 11:45:03', 'picture-1611137617187.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `KODE` int(15) NOT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `KODE_PRODUK` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`KODE`, `EMAIL`, `KODE_PRODUK`) VALUES
(42, '::1', 1),
(43, '::1', 1),
(44, '::1', 1),
(47, '::1', 1),
(48, '::1', 1),
(49, '::1', 1),
(50, '::1', 1),
(52, '::1', 1),
(53, '::1', 1),
(66, '::1', 1),
(67, '::1', 1),
(68, '::1', 1),
(69, '::1', 2),
(70, '::1', 2),
(71, '::1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_transaksi`
--

CREATE TABLE `tbl_detail_transaksi` (
  `KODE` int(15) NOT NULL,
  `KODE_TRANSAKSI` int(15) NOT NULL,
  `KODE_PRODUK` int(15) NOT NULL,
  `JUMLAH` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_detail_transaksi`
--

INSERT INTO `tbl_detail_transaksi` (`KODE`, `KODE_TRANSAKSI`, `KODE_PRODUK`, `JUMLAH`) VALUES
(1, 6, 1, 1),
(2, 6, 2, 3),
(3, 14, 3, 2),
(4, 15, 1, 1),
(5, 16, 1, 1),
(6, 17, 1, 1),
(7, 18, 1, 2),
(8, 19, 1, 1),
(9, 20, 2, 3),
(10, 21, 1, 1),
(11, 22, 1, 6),
(12, 22, 2, 7),
(13, 22, 3, 3),
(14, 22, 0, 0),
(15, 23, 1, 6),
(16, 23, 2, 7),
(17, 23, 3, 3),
(18, 23, 0, 0),
(19, 24, 1, 6),
(20, 24, 2, 7),
(21, 24, 3, 3),
(22, 24, 0, 0),
(23, 25, 1, 1),
(24, 26, 1, 6),
(25, 26, 2, 7),
(26, 26, 3, 3),
(27, 26, 0, 0),
(28, 36, 1, 6),
(30, 36, 3, 3),
(31, 36, 0, 0),
(35, 36, 2, 7),
(37, 38, 1, 9),
(38, 38, 0, 0),
(39, 39, 1, 9),
(40, 39, 0, 0),
(41, 40, 1, 9),
(42, 40, 0, 0),
(43, 41, 1, 9),
(44, 41, 0, 0),
(45, 42, 1, 9),
(46, 42, 0, 0),
(47, 43, 1, 3),
(48, 44, 1, 2),
(49, 45, 1, 4),
(50, 45, 2, 2),
(51, 45, 0, 0),
(52, 46, 1, 2),
(53, 47, 1, 5),
(54, 47, 2, 2),
(55, 47, 0, 0),
(56, 48, 1, 1),
(57, 36, 4, 1),
(58, 49, 1, 12),
(59, 49, 2, 3),
(60, 49, 0, 0),
(61, 50, 1, 2),
(62, 50, 2, 2),
(63, 50, 0, 0),
(64, 51, 1, 1),
(65, 52, 1, 1),
(66, 53, 0, 0),
(67, 54, 1, 1),
(68, 55, 1, 1),
(69, 56, 5, 1),
(70, 56, 0, 0),
(71, 57, 5, 1),
(72, 57, 0, 0),
(73, 58, 1, 2),
(74, 58, 2, 1),
(75, 58, 5, 1),
(76, 58, 0, 0),
(77, 59, 1, 1),
(78, 60, 1, 1),
(79, 60, 2, 1),
(80, 60, 0, 0),
(81, 61, 1, 1),
(82, 61, 2, 1),
(83, 61, 0, 0),
(84, 62, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_galeri`
--

CREATE TABLE `tbl_galeri` (
  `KODE` int(5) NOT NULL,
  `GAMBAR` varchar(100) NOT NULL,
  `KETERANGAN` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_galeri`
--

INSERT INTO `tbl_galeri` (`KODE`, `GAMBAR`, `KETERANGAN`) VALUES
(1, 'akurat_20200813021738_2Z53US.jpg', '1'),
(2, 'picture-1602752303382.jpg', '2'),
(3, 'picture-1602752330187.png', '3'),
(4, 'picture-1602752827396.jpg', 'sdf'),
(5, 'picture-1602752846296.jpg', 'sadf'),
(6, 'picture-1602752867483.jpeg', 'sdfdf'),
(7, 'single_product.png', 'sdfsdfdsf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_history`
--

CREATE TABLE `tbl_history` (
  `KODE` int(11) NOT NULL,
  `DIBUAT` datetime DEFAULT NULL,
  `DIBAYAR` datetime DEFAULT NULL,
  `DIKIRIM` datetime DEFAULT NULL,
  `DITERIMA` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_history`
--

INSERT INTO `tbl_history` (`KODE`, `DIBUAT`, `DIBAYAR`, `DIKIRIM`, `DITERIMA`) VALUES
(46, '2020-12-15 18:57:40', NULL, NULL, '2020-12-24 12:10:53'),
(47, '2021-01-14 14:46:26', '2021-01-15 14:46:26', '2021-01-15 17:50:42', '2021-01-19 15:48:29'),
(50, '2020-12-16 12:46:56', '2020-12-16 12:48:23', '2021-01-13 17:06:33', NULL),
(52, '2020-12-16 13:05:35', '2020-12-16 13:09:11', '2020-12-24 11:05:03', '2020-12-24 12:10:53'),
(58, '2020-12-24 11:15:47', NULL, NULL, '2020-12-24 12:10:53'),
(59, '2020-12-24 11:18:23', NULL, NULL, '2020-12-24 12:10:53'),
(60, '2020-12-24 11:19:35', NULL, NULL, '2020-12-24 12:10:53'),
(61, '2020-12-24 19:10:52', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `KODE` int(5) NOT NULL,
  `NAMA` varchar(30) NOT NULL,
  `GAMBAR` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`KODE`, `NAMA`, `GAMBAR`) VALUES
(1, 'KATEGORI 1', 'tempe-kedelai.jpg'),
(2, 'KATEGORI 2', 'tempe-gembus.jpg'),
(3, 'KATEGORI 3', 'tempe-lupin.jpeg'),
(4, 'KATEGORI 4', 'tempe-lupin.jpeg'),
(5, 'KATEGORI 5', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `KODE` int(15) NOT NULL,
  `NAMA` varchar(30) NOT NULL,
  `HARGA` int(10) NOT NULL,
  `STOK` int(7) NOT NULL,
  `KATEGORI` varchar(30) NOT NULL,
  `GAMBAR` varchar(100) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `TANGGAL_INPUT` timestamp NOT NULL DEFAULT current_timestamp(),
  `JUMLAH_DIPESAN` int(7) NOT NULL,
  `KODE_KATEGORI` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`KODE`, `NAMA`, `HARGA`, `STOK`, `KATEGORI`, `GAMBAR`, `DESKRIPSI`, `TANGGAL_INPUT`, `JUMLAH_DIPESAN`, `KODE_KATEGORI`) VALUES
(1, 'TEMPE GEMBUS', 5000, 0, 'TEMPE LUPIN', 'picture-1602752303382.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2020-12-16 06:25:26', 15, 1),
(2, 'TEMPE KEDELAI', 1000, 3, 'TEMPE KEDELAI', 'picture-1602752330187.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2020-12-16 06:28:57', 7, 1),
(3, 'TEMPE LUPIN', 3000, 5, 'TEMPE LUPIN', 'single_product.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2020-12-16 06:25:26', 0, 2),
(4, 'TEMPE BACEM', 1000, 5, 'TEMPE BACEM', 'picture-1602752827396.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2020-12-16 06:25:26', 5, 3),
(5, 'TEMPE LUPIN 3', 5000, 5, '4', 'picture-1611139575055.png', '<p><strong>tes</strong></p>\r\n\r\n<p><strong><em>tes</em></strong></p>\r\n\r\n<p><strong><em><s>tes</s></em></strong></p>\r\n', '2020-12-16 06:29:05', 0, 5),
(6, 'TEMPE GAMBUS', 3000, 5, 'TEMPE LUPIN', 'picture-1602752867483.jpeg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2020-12-16 06:25:26', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `KODE` int(15) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `WAKTU` timestamp NOT NULL DEFAULT current_timestamp(),
  `TOTAL` int(15) NOT NULL,
  `PENERIMA` varchar(50) NOT NULL,
  `TELEPON` varchar(15) NOT NULL,
  `EMAIL_PENERIMA` varchar(100) NOT NULL,
  `ALAMAT` text NOT NULL,
  `KOTA` varchar(50) NOT NULL,
  `ONGKIR` int(10) NOT NULL,
  `SERVICE` varchar(30) NOT NULL,
  `STATUS` varchar(15) NOT NULL,
  `RESI` varchar(30) NOT NULL,
  `TOKEN` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`KODE`, `EMAIL`, `WAKTU`, `TOTAL`, `PENERIMA`, `TELEPON`, `EMAIL_PENERIMA`, `ALAMAT`, `KOTA`, `ONGKIR`, `SERVICE`, `STATUS`, `RESI`, `TOKEN`) VALUES
(6, 'tes@tes.com', '2020-10-19 05:26:45', 8000, '', '', '', '', '', 0, '', '', '', ''),
(7, 'undefined', '2020-10-19 05:28:26', 5000, '', '', '', '', '', 0, '', '', '', ''),
(8, 'undefined', '2020-10-19 05:31:42', 5000, '', '', '', '', '', 0, '', '', '', ''),
(9, 'undefined', '2020-10-19 05:32:17', 15000, '', '', '', '', '', 0, '', '', '', ''),
(10, 'undefined', '2020-10-19 05:34:48', 10000, '', '', '', '', '', 0, '', '', '', ''),
(11, 'undefined', '2020-10-19 05:35:41', 10000, '', '', '', '', '', 0, '', '', '', ''),
(12, 'undefined', '2020-10-19 05:37:04', 15000, '', '', '', '', '', 0, '', '', '', ''),
(13, 'undefined', '2020-10-19 05:37:59', 15000, '', '', '', '', '', 0, '', '', '', ''),
(14, 'undefined', '2020-12-01 10:09:41', 2, '', '', '', '', '', 0, '', '', '', ''),
(15, 'tes@tes.com', '2020-12-01 10:15:12', 1, '', '', '', '', '', 0, '', '', '', ''),
(16, 'undefined', '2020-12-01 10:17:05', 1, '', '', '', '', '', 0, '', '', '', ''),
(17, 'undefined', '2020-12-01 10:17:35', 1, '', '', '', '', '', 0, '', '', '', ''),
(18, 'undefined', '2020-12-01 10:19:37', 10000, '', '', '', '', '', 0, '', '', '', ''),
(19, 'undefined', '2020-12-01 10:21:18', 5000, '', '', '', '', '', 0, '', '', '', ''),
(20, 'tes@tes.com', '2020-12-01 10:22:54', 3000, '', '', '', '', '', 0, '', '', '', ''),
(21, 'undefined', '2020-12-01 11:58:22', 5000, '', '', '', '', '', 0, '', '', '', ''),
(22, 'tes@tes.com', '2020-12-01 12:24:12', 0, '', '', '', '', '', 0, '', '', '', ''),
(23, 'tes@tes.com', '2020-12-01 12:26:33', 0, '', '', '', '', '', 0, '', '', '', ''),
(24, 'tes@tes.com', '2020-12-01 12:27:34', 0, '', '', '', '', '', 0, '', '', '', ''),
(25, 'undefined', '2020-12-01 12:27:53', 5000, '', '', '', '', '', 0, '', '', '', ''),
(26, 'tes@tes.com', '2020-12-01 12:28:24', 0, 'Iqbal Dwi Ardiansyah', '081220700605', '', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', ''),
(36, 'tes@tes.com', '2020-12-01 12:31:01', 47000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'PAID', 'JNCL-7127560338', ''),
(38, 'undefined', '2020-12-06 03:15:19', 45000, 'Iqbal Dwi Ardiansyah', '081220700605', '', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', ''),
(39, 'undefined', '2020-12-06 03:18:13', 45000, 'Iqbal Dwi Ardiansyah', '081220700605', '', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', ''),
(40, 'undefined', '2020-12-06 03:20:17', 45000, 'Iqbal Dwi Ardiansyah', '081220700605', '', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', ''),
(41, 'undefined', '2020-12-06 03:28:14', 45000, 'Iqbal Dwi Ardiansyah', '081220700605', '', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', '8d4387a6-bff6-48a3-9bb2-0b67dbe43ae6'),
(42, 'undefined', '2020-12-06 03:31:08', 45000, '', '081220700605', '', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', '7c9fd73c-0350-4cca-8551-2b778076041f'),
(43, 'tes@tes.com', '2020-12-06 06:24:34', 15000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 10000, 'YES', 'UNPAID', '', '0c27a400-01d2-45e4-8f6e-04d5ed8edd99'),
(44, 'tes@tes.com', '2020-12-06 06:25:40', 10000, '', '', '', '', '', 0, '', '', '', ''),
(45, 'tes@tes.com', '2020-12-06 06:54:10', 22000, '', '', '', '', '', 0, '', '', '', ''),
(46, 'tes@tes.com', '2020-12-06 07:19:11', 10000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 10000, 'YES', 'CANCEL', '', 'd924e794-8321-4ee6-9b1c-ff67f95367ee'),
(47, 'tes@tes.com', '2020-12-07 06:07:04', 27000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'DONE', 'JNCL-7127560338', 'f5d993a3-b89f-4a6a-b0da-152b3ca8c1ee'),
(48, 'tes@tes.com', '2020-12-07 06:15:54', 5000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 8000, 'REG', 'UNPAID', '', 'f11b5677-a0e6-4c97-9dae-53c6a6e5cec6'),
(49, 'undefined', '2020-12-10 09:58:19', 63000, '', '', '', '', '', 0, '', '', '', ''),
(50, 'tes@tes.com', '2020-12-16 05:46:37', 12000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 10000, 'YES', 'SEND', 'PLMAA16157522118', 'e98a5e26-3125-4e8c-9fc4-2cac58b43821'),
(51, 'tes@tes.com', '2020-12-16 05:51:02', 5000, '', '', '', '', '', 0, '', '', '', ''),
(52, 'tes@tes.com', '2020-12-16 06:05:27', 5000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kabupaten Bandung', 10000, 'YES', 'DONE', '', 'bba4ad45-1823-4bcf-97f6-6c2e315551a6'),
(53, 'tes@tes.com', '2020-12-16 06:19:46', 0, '', '', '', '', '', 0, '', '', '', ''),
(54, 'tes@tes.com', '2020-12-17 07:44:53', 5000, '', '', '', '', '', 0, '', '', '', ''),
(55, 'tes@tes.com', '2020-12-17 07:59:37', 5000, '', '', '', '', '', 0, '', '', '', ''),
(56, 'tes@tes.com', '2020-12-17 10:20:33', 5000, '', '', '', '', '', 0, '', '', '', ''),
(57, 'tes@tes.com', '2020-12-22 11:00:23', 5000, '', '', '', '', '', 0, '', '', '', ''),
(58, 'tes@tes.com', '2020-12-24 04:15:37', 16000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kota Bandung', 8000, 'CTC', 'CANCEL', '', '6e9d3536-0f0d-434a-8a91-3a98d65a28ed'),
(59, 'tes@tes.com', '2020-12-24 04:18:18', 5000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kota Bandung', 8000, 'CTC', 'UNPAID', '', '03e66418-25c1-4762-a1fa-cb60e2f2e377'),
(60, 'tes@tes.com', '2020-12-24 04:19:30', 6000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kota Bandung', 8000, 'CTC', 'UNPAID', '', 'abc40697-c4fd-4aaf-aea2-f77ce9a37341'),
(61, 'tes@tes.com', '2020-12-24 12:10:07', 6000, 'Iqbal Dwi Ardiansyah', '081220700605', 'tes@tes.com', 'Jl.kencanawangi XII Blok.P No.19', 'Kota Bandung', 8000, 'CTC', 'UNPAID', '', '0f348560-89d1-49de-b03b-3a8c3ca2aba0'),
(62, 'tes@tes.com', '2021-01-04 10:37:13', 5000, '', '', '', '', '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tes_esp`
--

CREATE TABLE `tes_esp` (
  `kode` int(5) NOT NULL,
  `soil` varchar(5) DEFAULT NULL,
  `suhu` varchar(5) DEFAULT NULL,
  `ph` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`EMAIL`);

--
-- Indexes for table `tbl_akun`
--
ALTER TABLE `tbl_akun`
  ADD PRIMARY KEY (`EMAIL`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tbl_detail_transaksi`
--
ALTER TABLE `tbl_detail_transaksi`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tbl_galeri`
--
ALTER TABLE `tbl_galeri`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tbl_history`
--
ALTER TABLE `tbl_history`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`KODE`);

--
-- Indexes for table `tes_esp`
--
ALTER TABLE `tes_esp`
  ADD PRIMARY KEY (`kode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `KODE` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tbl_detail_transaksi`
--
ALTER TABLE `tbl_detail_transaksi`
  MODIFY `KODE` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `tbl_galeri`
--
ALTER TABLE `tbl_galeri`
  MODIFY `KODE` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `KODE` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `KODE` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `KODE` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tes_esp`
--
ALTER TABLE `tes_esp`
  MODIFY `kode` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
