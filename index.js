const path = require('path');
const hbs = require('ejs');
const bodyParser = require('body-parser');

const http = require('http');
const socket = require('socket.io');
const express = require('express');
const session = require('express-session');
const app = express();
const server = http.createServer(app);
const io = socket(server);

const expressip = require('express-ip'); //get client ip
app.use(expressip().getIpInfoMiddleware); //get client ip

const multer = require('multer');
const storage = multer.diskStorage({
    destination : path.join(__dirname + '/public/img/product'),
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() +
        path.extname(file.originalname));
    }
});

//const multer = require('multer');
const lokasi = multer.diskStorage({
    destination : path.join(__dirname + '/public/img/user'),
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() +
        path.extname(file.originalname));
    }
});

const lokasi_kategori = multer.diskStorage({
    destination : path.join(__dirname + '/public/img/categori'),
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() +
        path.extname(file.originalname));
    }
});

const upload = multer({
    storage : storage
}).single('picture');

const upload_profil = multer({
    storage : lokasi
}).single('picture');

const upload_kategori = multer({
    storage : lokasi_kategori
}).single('picture');

const fs = require('fs');

//var key_rajaongkir = "c3095aff2b7487c569b16ae5f047b650"; // akun 2
var key_rajaongkir = "42cfdd8f36675e7233d754fcde2bc14b"; // akun 1
//var key_rajaongkir = "870882e64fca9597a47971f82bebe87b"; // akun 3


app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views'));
server.listen(process.env.PORT || 3000, () => console.log('server on port 3000'));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/assets',express.static(__dirname + '/public'));
app.use('/main',express.static(__dirname + '/'));


app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));


var conn = require("./config_db");
 
conn.connect((err) =>{
	if(err) throw err;
		conn.query("SELECT * FROM tbl_produk", function (err, result, fields) {
	    	if (err) throw err;
	    	//console.log(result);
  		});

  	// 	conn.query("SELECT * FROM tbl_gps order by WAKTU desc limit 1", function (err, result, fields) {
	  //   	if (err) throw err;
	  //   	let data = JSON.stringify(result);
			// fs.writeFileSync('./public/data_gps.json', data);
  	// 	});

	console.log('Mysql Connected...');
});

app.get('/admin', function(req, res) {
	res.render('./admin/login',{});
});

app.get('/simple_checkout', function (req, res) {
  // initialize snap client object
  const midtransClient = require('midtrans-client');
  let snap = new midtransClient.Snap({
    isProduction : false,
    serverKey : 'SB-Mid-server-SW30BYMKE3PJw3-1mP3iyPQP',
    clientKey : 'SB-Mid-client-S6Mmt2K_NFERxpzQ'
  });
  let parameter = {
    "transaction_details": {
      "order_id": "order-id-node-"+Math.round((new Date()).getTime() / 1000),
      "gross_amount": 200000
    }, "credit_card":{
      "secure" : true
    }
  };
  // create snap transaction token
  snap.createTransactionToken(parameter)
    .then((transactionToken)=>{
        // pass transaction token to frontend
        res.render('simple_checkout',{
          token: transactionToken, 
          clientKey: snap.apiConfig.clientKey,
          token2: "8d4387a6-bff6-48a3-9bb2-0b67dbe43ae6"
        })
    })
})



app.post('/notification_handler', function(req, res){
  const midtransClient = require('midtrans-client');
  let receivedJson = req.body;

  let core = new midtransClient.CoreApi({
	  isProduction : false,
	  serverKey : 'SB-Mid-server-SW30BYMKE3PJw3-1mP3iyPQP',
	  clientKey : 'SB-Mid-client-S6Mmt2K_NFERxpzQ'
	});

  core.transaction.notification(receivedJson)
    .then((transactionStatusObject)=>{
      let orderId = transactionStatusObject.order_id;
      let transactionStatus = transactionStatusObject.transaction_status;
      let fraudStatus = transactionStatusObject.fraud_status;

      let summary = `Transaction notification received. Order ID: ${orderId}. Transaction status: ${transactionStatus}. Fraud status: ${fraudStatus}.<br>Raw notification object:<pre>${JSON.stringify(transactionStatusObject, null, 2)}</pre>`;

      // [5.B] Handle transaction status on your backend via notification alternatively
      // Sample transactionStatus handling logic
      if (transactionStatus == 'capture'){
          if (fraudStatus == 'challenge'){
              // TODO set transaction status on your databaase to 'challenge'
          } else if (fraudStatus == 'accept'){
              // TODO set transaction status on your databaase to 'success'
          }
      } else if (transactionStatus == 'settlement'){
        console.log("berhasil");
      } else if (transactionStatus == 'cancel' ||
        transactionStatus == 'deny' ||
        transactionStatus == 'expire'){
        // TODO set transaction status on your databaase to 'failure'
      } else if (transactionStatus == 'pending'){
      	console.log("pending");
        // TODO set transaction status on your databaase to 'pending' / waiting payment
      } else if (transactionStatus == 'refund'){
        // TODO set transaction status on your databaase to 'refund'
      }
      console.log(summary);
      res.send(summary);
    });
})

app.get('/cek_pembayaran', function(req, res) {
//offline
	//if (req.session.loggedin) {
		console.log(req.query.order_id);
		console.log("cek");
		var request = require('request');
		var options = {
		  	'method': 'GET',
		  	'url': 'https://api.sandbox.midtrans.com/v2/'+req.query.order_id+'/status',
		  	'headers': {
		    	'Accept': 'application/json',
		    	'Content-Type': 'application/json',
		    	'Authorization': 'Basic U0ItTWlkLXNlcnZlci1TVzMwQllNS0UzUEp3My0xbVAzaXlQUVA6'
		  	}
		};
		request(options, function (error, response) {
		  	if (error) throw new Error(error);

			var json =  JSON.parse(response.body);
			console.log('>> json: ', json);

			if(json.transaction_status == "capture" || json.transaction_status == "settlement"){
				let sql = "UPDATE tbl_transaksi set STATUS = 'PAID' WHERE KODE = '"+req.query.order_id+"' ; ";
				let query = conn.query(sql, (err, results) => {
					let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; UPDATE tbl_history set DIBAYAR = CURRENT_TIMESTAMP() WHERE KODE = "+req.query.order_id+";";
					let query2 = conn.query(sql2, [4,1], (err, hasil) => {
						var kode_produk = hasil[2];
						//console.log(kode_produk);
						for(var i = 0;i<kode_produk.length;i++){
							var kode = kode_produk[i].KODE_PRODUK;
							var jumlah = kode_produk[i].JUMLAH;
							
							let sql3 = "UPDATE tbl_produk set STOK = STOK-"+jumlah+" WHERE KODE = '"+kode+"' ; ";
							let query3 = conn.query(sql3, [3,1], (err, hasil) => {})
						}
						
			     		res.render('pembayaran_berhasil',{
							keranjang: hasil[0][0],
							isi_keranjang: hasil[1][0],
							akun_email: req.session.email,
							login: "ada",
							foto:"tes.png"
						});
			     	})
				});
			}else{
				let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"'";
				let query2 = conn.query(sql2, [3,1], (err, hasil) => {
				console.log(data[115])
		     		res.render('pembayaran_gagal',{
						keranjang: hasil[0][0],
						isi_keranjang: hasil[1][0],
						akun_email: req.session.email,
						login: "ada",
						foto:"tes.png"
					});
		     	})
			}
		});
	// }else{
	// 	res.redirect('/');
	// }
});

// app.get('/cek_pembayaran', function(req, res) {
// online
// 	if (req.session.loggedin) {
// 		console.log(req.query.order_id);
// 		console.log("cek");
// 		var request = require('request');
// 		var options = {
// 		  	'method': 'GET',
// 		  	'url': 'https://api.sandbox.midtrans.com/v2/'+req.query.order_id+'/status',
// 		  	'headers': {
// 		    	'Accept': 'application/json',
// 		    	'Content-Type': 'application/json',
// 		    	'Authorization': 'Basic U0ItTWlkLXNlcnZlci1TVzMwQllNS0UzUEp3My0xbVAzaXlQUVA6'
// 		  	}
// 		};
// 		request(options, function (error, response) {
// 		  	if (error) throw new Error(error);

// 			var json =  JSON.parse(response.body);
// 			console.log('>> json: ', json);

// 			if(json.transaction_status == "capture" || json.transaction_status == "settlement"){
// 				let sql = "UPDATE tbl_transaksi set STATUS = 'PAID' WHERE KODE = '"+req.query.order_id+"' ; ";
// 				let query = conn.query(sql, (err, results) => {
// 					let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; UPDATE tbl_history set DIBAYAR = CURRENT_TIMESTAMP() WHERE KODE = "+req.query.order_id+";";
// 					let query2 = conn.query(sql2, [4,1], (err, hasil) => {
// 						var kode_produk = hasil[2];
// 						//console.log(kode_produk);
// 						for(var i = 0;i<kode_produk.length;i++){
// 							var kode = kode_produk[i].KODE_PRODUK;
// 							var jumlah = kode_produk[i].JUMLAH;
							
// 							let sql3 = "UPDATE tbl_produk set STOK = STOK-"+jumlah+" WHERE KODE = '"+kode+"' ; ";
// 							let query3 = conn.query(sql3, [3,1], (err, hasil) => {})
// 						}
						
// 			     		res.render('pembayaran_berhasil',{
// 							keranjang: hasil[0][0],
// 							isi_keranjang: hasil[1][0],
// 							akun_email: req.session.email,
// 							login: "ada",
// 							foto:hasil[2][0].FOTO
// 						});
// 			     	})
// 				});
// 			}else{
// 				let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"'";
// 				let query2 = conn.query(sql2, [3,1], (err, hasil) => {
// 				console.log(data[115])
// 		     		res.render('pembayaran_gagal',{
// 						keranjang: hasil[0][0],
// 						isi_keranjang: hasil[1][0],
// 						akun_email: req.session.email,
// 						login: "ada",
// 						foto:hasil[2][0].FOTO
// 					});
// 		     	})
// 			}
// 		});
// 	}else{
// 		res.redirect('/');
// 	}
// });

app.get('/confirmation', function(req, res) {
	res.render('confirmation',{});
});

app.get('/detail_pesanan', function(req, res) {
	if (req.session.loggedin) {
		
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' group by tbl_cart.KODE_PRODUK ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.KODE = '"+req.query.kode+"' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI; SELECT * FROM tbl_history WHERE KODE = '"+req.query.kode+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori ; ";
		let query2 = conn.query(sql2, [6,1], (err, hasil) => {

			var request = require('request');
			var options = {
			  'method': 'GET',
			  'url': 'https://api.cekresi.pigoora.com/cekResi?key=pigoorafreeservices&resi='+hasil[2][0].RESI,
			  'headers': {
			  }
			};
			

			function data_resi(callback){
				request(options, function (error, response, body) {
				  if (error) throw new Error(error);
				  data = JSON.parse(body)
				  callback(data);
				  //console.log(data.result);
				});
		  	}

		  	data_resi(function(data){

		  		if (hasil[2][0].RESI != "") {
		  			res.render('detail_transaksi',{
						keranjang: hasil[0][0],
						isi_keranjang: hasil[1],
						akun_email: req.session.email,
						login: "ada",
						produk: hasil[2][0],
						history: hasil[3][0],
						foto: hasil[4][0].FOTO,
						kategori_menu : hasil[5],
						lacak : data.result.manifest
					});
		  		}else{
		  			res.render('detail_transaksi',{
						keranjang: hasil[0][0],
						isi_keranjang: hasil[1],
						akun_email: req.session.email,
						login: "ada",
						produk: hasil[2][0],
						history: hasil[3][0],
						foto: hasil[4][0].FOTO,
						kategori_menu : hasil[5],
						lacak : data
					});
		  		}
		  	})
		})
	}else{
		res.redirect('/');
	}	
});


app.post('/login_admin', function(req, res) {
	var email = req.body.email;
	var password = req.body.password;
	if (email && password) {
		conn.query('SELECT * FROM tbl_admin WHERE EMAIL = ? AND PASSWORD = ?', [email, password], function(error, results, fields) {
			if (results.length > 0) {
				console.log(results);
				var string=JSON.stringify(results);
				var json =  JSON.parse(string);
				console.log('>> json: ', json);
				
				req.session.loggedin_admin = true;
				req.session.email_admin = email;

				console.log("login");
				res.redirect('/admin/dashboard');
				
			} else {
				console.log("gagal");
				res.redirect('/admin');
			}			
			res.end();
		});
	} else {
		res.send('Please enter Username and Password!');
		res.end();
	}
});

app.get('/admin/dashboard', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'PAID' ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'ON PROCESS' ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'SEND' ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'DONE' ; SELECT * from tbl_transaksi WHERE STATUS != '' && STATUS != 'UNPAID' && STATUS != 'CANCEL' ORDER BY WAKTU DESC limit 10 ; SELECT * from tbl_produk WHERE STOK < 5 limit 5; SELECT tbl_kategori.NAMA, SUM(t1.HARGA) as HARGA FROM ( SELECT tbl_detail_transaksi.JUMLAH * tbl_produk.HARGA as HARGA, tbl_produk.KODE_KATEGORI FROM tbl_detail_transaksi JOIN tbl_transaksi ON tbl_detail_transaksi.KODE_TRANSAKSI = tbl_transaksi.KODE JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS != '' && tbl_transaksi.STATUS != 'UNPAID' && tbl_transaksi.STATUS != 'CANCEL' ) as t1 RIGHT JOIN tbl_kategori ON t1.KODE_KATEGORI = tbl_kategori.KODE GROUP BY tbl_kategori.NAMA ; SELECT SUM(TOTAL) as TOTAL, DATE_FORMAT(WAKTU,'%Y-%m') as TANGGAL FROM tbl_transaksi WHERE tbl_transaksi.STATUS != '' && tbl_transaksi.STATUS != 'UNPAID' && tbl_transaksi.STATUS != 'CANCEL' GROUP BY TANGGAL ORDER BY TANGGAL ASC" ;
		let query = conn.query(sql, [8,1] ,(err, results) => {
			console.log(req.session.email_admin);
			res.render('./admin/index',{
				email_admin : req.session.email_admin,
				jumlah_belum_proses : results[0][0],
				jumlah_belum_kirim : results[1][0],
				jumlah_kirim : results[2][0],
				jumlah_selesai : results[3][0],
				pesanan : results[4],
				stok : results[5],
				pendapatan_kategori : results[6],
				pendapatan_tanggal : results[7]
			});
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/belum_diproses', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT * from tbl_transaksi WHERE STATUS = 'PAID' ; SELECT COUNT(KODE) as JUMLAH from tbl_transaksi WHERE STATUS = 'PAID' GROUP BY STATUS" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'PAID' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				console.log(hasil);
				res.render('./admin/belum_diproses',{
					email_admin : req.session.email_admin,
					pesanan : results[0],
					produk : hasil
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/belum_dikirim', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT * from tbl_transaksi WHERE STATUS = 'ON PROCESS' ; SELECT COUNT(KODE) as JUMLAH from tbl_transaksi WHERE STATUS = 'PAID' GROUP BY STATUS" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'ON PROCESS' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				console.log(hasil);
				res.render('./admin/belum_dikirim',{
					email_admin : req.session.email_admin,
					pesanan : results[0],
					produk : hasil
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/selesai', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT * from tbl_transaksi WHERE STATUS = 'DONE' ; SELECT COUNT(KODE) as JUMLAH from tbl_transaksi WHERE STATUS = 'PAID' GROUP BY STATUS" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'DONE' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				console.log(hasil);
				res.render('./admin/selesai',{
					email_admin : req.session.email_admin,
					pesanan : results[0],
					produk : hasil
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/daftar_barang', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT tbl_produk.KODE, tbl_produk.NAMA, tbl_produk.HARGA, tbl_kategori.KODE as KODE_KATEGORI, tbl_produk.DESKRIPSI, tbl_produk.STOK, tbl_produk.GAMBAR, tbl_produk.JUMLAH_DIPESAN, tbl_kategori.NAMA as KATEGORI from tbl_produk JOIN tbl_kategori ON tbl_produk.KODE_KATEGORI = tbl_kategori.KODE ; SELECT * from tbl_kategori" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'DONE' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				res.render('./admin/daftar_barang',{
					email_admin : req.session.email_admin,
					produk : results[0],
					detail_produk : hasil,
					kategori : results[1]
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/daftar_kategori', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT tbl_produk.KODE, tbl_produk.NAMA, tbl_produk.HARGA, tbl_kategori.KODE as KODE_KATEGORI, tbl_produk.DESKRIPSI, tbl_produk.STOK, tbl_produk.GAMBAR, tbl_produk.JUMLAH_DIPESAN, tbl_kategori.NAMA as KATEGORI from tbl_produk JOIN tbl_kategori ON tbl_produk.KODE_KATEGORI = tbl_kategori.KODE ; SELECT * from tbl_kategori" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'DONE' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				res.render('./admin/daftar_kategori',{
					email_admin : req.session.email_admin,
					produk : results[0],
					detail_produk : hasil,
					kategori : results[1]
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/daftar_stok', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT tbl_produk.KODE, tbl_produk.NAMA, tbl_produk.HARGA, tbl_kategori.KODE as KODE_KATEGORI, tbl_produk.DESKRIPSI, tbl_produk.STOK, tbl_produk.GAMBAR, tbl_produk.JUMLAH_DIPESAN, tbl_kategori.NAMA as KATEGORI from tbl_produk JOIN tbl_kategori ON tbl_produk.KODE_KATEGORI = tbl_kategori.KODE ; SELECT * from tbl_kategori" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'DONE' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				res.render('./admin/daftar_stok',{
					email_admin : req.session.email_admin,
					produk : results[0],
					detail_produk : hasil,
					kategori : results[1]
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/tambah_barang', function(req, res) {
	//if (req.session.loggedin_admin) {
		let sql = "SELECT tbl_produk.KODE, tbl_produk.NAMA, tbl_produk.HARGA, tbl_kategori.KODE as KODE_KATEGORI, tbl_produk.DESKRIPSI, tbl_produk.STOK, tbl_produk.GAMBAR, tbl_produk.JUMLAH_DIPESAN, tbl_kategori.NAMA as KATEGORI from tbl_produk JOIN tbl_kategori ON tbl_produk.KODE_KATEGORI = tbl_kategori.KODE ; SELECT * from tbl_kategori" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'DONE' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				res.render('./admin/tambah_produk',{
					email_admin : req.session.email_admin,
					produk : results[0],
					detail_produk : hasil,
					kategori : results[1]
				});
			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.get('/admin/tambah_kategori', function(req, res) {
	//if (req.session.loggedin_admin) {
		res.render('./admin/tambah_kategori',{
			email_admin : req.session.email_admin
		});
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.post('/admin/edit_barang', function(req, res){
	upload(req, res, err => {
		console.log(req.body.kategori)
       	if (err) throw err
       		var sql = "";
       	if (req.body.gambar == "-") {
       		if (req.body.kategori === undefined) {
       			sql = "UPDATE tbl_produk set NAMA = '"+req.body.nama+"',HARGA = '"+req.body.harga+"',STOK = '"+req.body.stok+"',GAMBAR = '"+req.file.filename+"',DESKRIPSI = '"+req.body.deskripsi+"' WHERE KODE = '"+req.body.kode+"'";
       		}else{
       			sql = "UPDATE tbl_produk set NAMA = '"+req.body.nama+"',HARGA = '"+req.body.harga+"',STOK = '"+req.body.stok+"',GAMBAR = '"+req.file.filename+"',KODE_KATEGORI = '"+req.body.kategori+"',DESKRIPSI = '"+req.body.deskripsi+"' WHERE KODE = '"+req.body.kode+"'";
       		}
       	}else{
       		if (req.body.kategori === undefined) {
       			sql = "UPDATE tbl_produk set NAMA = '"+req.body.nama+"',HARGA = '"+req.body.harga+"',STOK = '"+req.body.stok+"',GAMBAR = '"+req.body.gambar+"',DESKRIPSI = '"+req.body.deskripsi+"' WHERE KODE = '"+req.body.kode+"'";
       		}else{
       			sql = "UPDATE tbl_produk set NAMA = '"+req.body.nama+"',HARGA = '"+req.body.harga+"',STOK = '"+req.body.stok+"',GAMBAR = '"+req.body.gambar+"',KODE_KATEGORI = '"+req.body.kategori+"',DESKRIPSI = '"+req.body.deskripsi+"' WHERE KODE = '"+req.body.kode+"'";
       		}
       	}
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_barang');
       	})
    });
});

app.post('/admin/tambah_barang/save', function(req, res){
	upload(req, res, err => {
		console.log(req.body.kategori)
       	if (err) throw err
       		var sql = "";
       	if (req.body.gambar == "-") {
       		if (req.body.kategori === undefined) {
       			sql = "INSERT INTO tbl_produk (NAMA,HARGA,STOK,GAMBAR,DESKRIPSI,JUMLAH_DIPESAN,KODE_KATEGORI) VALUES ('"+req.body.nama+"','"+req.body.harga+"','"+req.body.stok+"','"+req.file.filename+"','"+req.body.deskripsi+"','0','"+req.body.kategori+"')";
       		}else{
       			sql = "INSERT INTO tbl_produk (NAMA,HARGA,STOK,GAMBAR,DESKRIPSI,JUMLAH_DIPESAN,KODE_KATEGORI) VALUES ('"+req.body.nama+"','"+req.body.harga+"','"+req.body.stok+"','"+req.file.filename+"','"+req.body.deskripsi+"','0','"+req.body.kategori+"')";
       		}
       	}else{
       		if (req.body.kategori === undefined) {
       			sql = "INSERT INTO tbl_produk (NAMA,HARGA,STOK,GAMBAR,DESKRIPSI,JUMLAH_DIPESAN,KODE_KATEGORI) VALUES ('"+req.body.nama+"','"+req.body.harga+"','"+req.body.stok+"','"+req.body.gambar+"','"+req.body.deskripsi+"','0','"+req.body.kategori+"')";
       		}else{
       			sql = "INSERT INTO tbl_produk (NAMA,HARGA,STOK,GAMBAR,DESKRIPSI,JUMLAH_DIPESAN,KODE_KATEGORI) VALUES ('"+req.body.nama+"','"+req.body.harga+"','"+req.body.stok+"','"+req.body.gambar+"','"+req.body.deskripsi+"','0','"+req.body.kategori+"')";
       		}
       	}
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_barang');
       	})
    });
});

app.post('/admin/hapus_barang', function(req, res){
	let sql = "DELETE from tbl_produk WHERE KODE = '"+req.body.kode+"'";
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_barang');
       	})
});

app.post('/admin/edit_kategori', function(req, res){
	upload_kategori(req, res, err => {
       	if (err) throw err
       		var sql = "";
       	if (req.body.gambar == "-") {
   			sql = "UPDATE tbl_kategori set NAMA='"+req.body.nama+"', GAMBAR = '"+req.file.filename+"' WHERE KODE = '"+req.body.kode+"'";
       	}else{
       		sql = "UPDATE tbl_kategori set NAMA='"+req.body.nama+"', GAMBAR = '"+req.body.gambar+"' WHERE KODE = '"+req.body.kode+"'";
       	}
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_kategori');
       	})
    });
});

app.post('/admin/tambah_kategori/save', function(req, res){
	upload_kategori(req, res, err => {
       	if (err) throw err
       		var sql = "";
       	if (req.body.gambar == "-") {
   			sql = "INSERT into tbl_kategori (NAMA,GAMBAR) VALUES ('"+req.body.nama+"','"+req.file.filename+"')";
       	}else{
       		sql = "INSERT into tbl_kategori (NAMA,GAMBAR) VALUES ('"+req.body.nama+"','"+req.body.gambar+"')";
       	}
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_kategori');
       	})
    });
});

app.post('/admin/hapus_kategori', function(req, res){
	let sql = "DELETE from tbl_kategori WHERE KODE = '"+req.body.kode+"'";
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_kategori');
       	})
});

app.post('/admin/tambah_stok', function(req, res){
	console.log(req.body.stok_tambahan);
	let sql = "UPDATE tbl_produk set STOK = STOK+"+req.body.stok_tambahan+" WHERE KODE = '"+req.body.kode+"'";
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/daftar_stok');
       	})
});

app.post('/admin/tambah_stok_dashboard', function(req, res){
	console.log(req.body.stok_tambahan);
	let sql = "UPDATE tbl_produk set STOK = STOK+"+req.body.stok_tambahan+" WHERE KODE = '"+req.body.kode+"'";
       	conn.query(sql, function(err, results){
       		res.redirect('/admin/dashboard');
       	})
});

app.get('/admin/dalam_pengiriman', function(req, res) {
	//if (req.session.loggedin_admin) {

		let sql = "SELECT * from tbl_transaksi WHERE STATUS = 'SEND' ; SELECT COUNT(KODE) as JUMLAH from tbl_transaksi WHERE STATUS = 'PAID' GROUP BY STATUS" ;
		let query = conn.query(sql, [2,1] ,(err, results) => {
			let sql2 = "SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.PENERIMA, tbl_transaksi.TELEPON, tbl_transaksi.EMAIL_PENERIMA, tbl_transaksi.ALAMAT, tbl_transaksi.KOTA, tbl_transaksi.SERVICE, tbl_transaksi.RESI, tbl_transaksi.TOKEN, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.STATUS = 'SEND' GROUP BY tbl_detail_transaksi.KODE_TRANSAKSI";
			let query = conn.query(sql2, [2,1] ,(err, hasil) => {
				//console.log(hasil);
				var lacak = [];
				for(var i=0;i<results[0].length;i++){
					var request = require('request');
					var options = {
					  'method': 'GET',
					  'url': 'https://api.cekresi.pigoora.com/cekResi?key=pigoorafreeservices&resi='+results[0][i].RESI,
					  'headers': {
					  }
					};
					

					function data_resi(callback){
						request(options, function (error, response, body) {
						  if (error) throw new Error(error);
						  data = JSON.parse(body)
						  //lacak.push("a");
						  //console.log(lacak);
						  callback(data);


						});

				  	}

				  	console.log(i+"-"+results[0].length);

				  	if (i == results[0].length-1) {
				  		data_resi(function(data){
					  		lacak.push(data.result.manifest) ;
					  		console.log(lacak)
					  		res.render('./admin/dalam_pengiriman',{
								email_admin : req.session.email_admin,
								pesanan : results[0],
								produk : hasil,
								lacak : lacak
							});
					  	})	
				  	}else{
				  		data_resi(function(data){
					  		lacak.push(data.result.manifest) ;
					  	})	
				  	}
			  	}

			})
		})
	//}else{
	//	res.redirect('/admin');
	//}
	
});

app.post('/admin/tes', function(req, res) {
	var url = require('url');
	var adr = 'https://api.cekresi.pigoora.com/cekResi?key=pigoorafreeservices&resi=PLMAA16157522118';
	//Parse the address:
	var q = url.parse(adr, true);

	/*The parse method returns an object containing url properties*/
	console.log(q.host);
	console.log(q.pathname);
	console.log(q.search);

	/*The query property returns an object with all the querystring parameters as properties:*/
	var qdata = q.query;
	console.log(qdata.month);
});

app.post('/admin/proses_all', function(req, res) {
	var text_kode = req.body.selected_kode;
	var kode = text_kode.split(";");
	kode.pop()

	for(var i=0;i<kode.length;i++){
		let sql = "UPDATE tbl_transaksi SET STATUS = 'ON PROCESS' WHERE KODE = '"+kode[i]+"'";
		let query = conn.query(sql, [2,1] ,(err, results) => {

		})
	}

	res.redirect("/admin/belum_diproses");
});

app.post('/admin/proses_pesanan', function(req, res) {
	let sql = "UPDATE tbl_transaksi SET STATUS = 'ON PROCESS' WHERE KODE = '"+req.body.kode+"'";
	let query = conn.query(sql, [2,1] ,(err, results) => {
		res.redirect("/admin/belum_diproses");
	})
});

app.post('/admin/input_resi', function(req, res) {
	let sql = "UPDATE tbl_transaksi SET STATUS = 'SEND', RESI = '"+req.body.resi+"' WHERE KODE = '"+req.body.kode+"' ; UPDATE tbl_history SET DIKIRIM = CURRENT_TIMESTAMP() WHERE KODE = '"+req.body.kode+"'";
	let query = conn.query(sql, [2,1] ,(err, results) => {
		res.redirect("/admin/belum_dikirim");
	})
});

app.post('/admin/pesanan_selesai', function(req, res) {
	let sql = "UPDATE tbl_transaksi SET STATUS = 'DONE' WHERE KODE = '"+req.body.kode+"' ; UPDATE tbl_history SET DITERIMA = CURRENT_TIMESTAMP() WHERE KODE = '"+req.body.kode+"'";
	let query = conn.query(sql, [2,1] ,(err, results) => {
		res.redirect("/admin/dalam_pengiriman");
	})
});

app.get('/admin/semua_pesanan', function(req, res) {
	let sql = "SELECT * from tbl_transaksi ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'PAID' ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'ON PROCESS' ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'SEND' ; SELECT COUNT(STATUS) as JUMLAH from tbl_transaksi WHERE STATUS = 'DONE'";
	let query = conn.query(sql, [1,1],(err, results) => {
		res.render('./admin/semua_pesanan',{
			email_admin : req.session.email_admin,
			pesanan : results[0],
			jumlah_belum_proses : results[1][0],
			jumlah_belum_kirim : results[2][0],
			jumlah_kirim : results[3][0],
			jumlah_selesai : results[4][0],
		});
	})
});

app.get('/', function(req, res) {
	let sql = "select * from tbl_produk ; SELECT * FROM tbl_produk ORDER BY JUMLAH_DIPESAN DESC ; SELECT * FROM tbl_produk ORDER BY TANGGAL_INPUT DESC ; SELECT KODE, LOWER(NAMA) as NAMA, GAMBAR FROM tbl_kategori limit 3";
	let query = conn.query(sql, [4,1] ,(err, results) => {
			
			console.log(results[3]);//get client ip
			
			if (req.session.loggedin) {
				let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
				let query2 = conn.query(sql2, [3,1],(err, hasil) => {
					
					res.render('index',{
						results: results[0],
						terlaris: results[1],
						terbaru: results[2],
						keranjang: hasil[0][0],
						akun_email: req.session.email,
						login : "ada",
						foto : hasil[1][0].FOTO,
						kategori_menu : hasil[2],
						kategori : results[3]

					});
				})
				
			}else{
				let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
				let query2 = conn.query(sql2,[2,1], (err, hasil) => {
					
					res.render('index',{
						results: results[0],
						terlaris: results[1],
						terbaru: results[2],
						keranjang: hasil[0],
						akun_email: req.ipInfo.ip,
						login : "kosong",
						foto : "picture-1608628151526.png",
						kategori_menu : hasil[1],
						kategori : results[3]
					});
				})
			}
	})
});

app.get('/kontak', function(req, res) {
	if (req.session.loggedin) {
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query2 = conn.query(sql2, [3,1],(err, hasil) => {
			res.render('kontak',{
				keranjang: hasil[0][0],
				akun_email: req.session.email,
				login : "ada",
				foto : hasil[1][0].FOTO,
				kategori_menu : hasil[2]

			});
		})
	}else{
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query2 = conn.query(sql2, [2,1],(err, hasil) => {
			res.render('kontak',{
				keranjang: hasil[0],
				akun_email: req.ipInfo.ip,
				login : "kosong",
				foto : "picture-1608628151526.png",
				kategori_menu : hasil[1]
			});
		})
	}
});

app.get('/galeri', function(req, res) {
	if (req.session.loggedin) {
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT * from tbl_galeri ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query2 = conn.query(sql2, [4,1],(err, hasil) => {
			res.render('galeri',{
				keranjang: hasil[0][0],
				akun_email: req.session.email,
				login : "ada",
				foto : hasil[1][0].FOTO,
				galeri : hasil[2],
				kategori_menu : hasil[3]

			});
		})
	}else{
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; SELECT * from tbl_galeri ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query2 = conn.query(sql2,[3,1], (err, hasil) => {
			res.render('galeri',{
				keranjang: hasil[0],
				akun_email: req.ipInfo.ip,
				login : "kosong",
				foto : "picture-1608628151526.png",
				galeri : hasil[1],
				kategori_menu : hasil[2]
			});
		})
	}
});

app.get('/cari', function(req, res) {

	if (req.session.loggedin) {
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_produk WHERE NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query2 = conn.query(sql2, [4,1],(err, hasil) => {
			res.render('cari',{
				keranjang: hasil[0][0],
				akun_email: req.session.email,
				login : "ada",
				foto : hasil[1][0].FOTO,
				produk : hasil[2],
				keyword : req.query.keyword,
				sorting : req.query.sorting,
				kondisi : req.query.kondisi,
				kategori_menu : hasil[3]

			});
		})
	}else{
		let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; SELECT * FROM tbl_produk WHERE NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query2 = conn.query(sql2,[3,1], (err, hasil) => {
			//console.log(hasil[1])
			res.render('cari',{
				keranjang: hasil[0],
				akun_email: req.ipInfo.ip,
				login : "kosong",
				foto : "picture-1608628151526.png",
				produk : hasil[1],
				keyword : req.query.keyword,
				sorting : req.query.sorting,
				kondisi : req.query.kondisi,
				kategori_menu : hasil[2]
			});
		})
	}
});

app.get('/semua_kategori', function(req, res) {
	let sql = "SELECT KODE, LOWER(NAMA) as NAMA, GAMBAR FROM tbl_kategori";
	let query = conn.query(sql, (err, results) => {
		if (req.session.loggedin) {
			let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_produk WHERE NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
			let query2 = conn.query(sql2, [4,1],(err, hasil) => {
				console.log(hasil[3]);
				res.render('semua_kategori',{
					keranjang: hasil[0][0],
					akun_email: req.session.email,
					login : "ada",
					foto : hasil[1][0].FOTO,
					produk : hasil[2],
					keyword : req.query.keyword,
					sorting : req.query.sorting,
					kondisi : req.query.kondisi,
					kategori_menu : hasil[3][0],
					kategori : results

				});
			})
		}else{
			let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; SELECT * FROM tbl_produk WHERE NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
			let query2 = conn.query(sql2,[3,1], (err, hasil) => {
				//console.log(hasil[1]);
				res.render('semua_kategori',{
					keranjang: hasil[0],
					akun_email: req.ipInfo.ip,
					login : "kosong",
					foto : "picture-1608628151526.png",
					produk : hasil[1],
					keyword : req.query.keyword,
					sorting : req.query.sorting,
					kondisi : req.query.kondisi,
					kategori_menu : hasil[2],
					kategori : results
				});
			})
		}
	})
});

app.get('/kategori', function(req, res) {
	let sql = "SELECT * from tbl_produk WHERE KODE_KATEGORI = '"+req.query.KODE+"' && NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA, GAMBAR FROM tbl_kategori ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori WHERE KODE = '"+req.query.KODE+"' ;";
	let query = conn.query(sql, [3,1], (err, results) => {
		if (req.session.loggedin) {
			let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_produk WHERE NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
			let query2 = conn.query(sql2, [4,1],(err, hasil) => {
				console.log(results);
				res.render('kategori',{
					keranjang: hasil[0][0],
					akun_email: req.session.email,
					login : "ada",
					foto : hasil[1][0].FOTO,
					produk : results[0],
					keyword : req.query.keyword,
					sorting : req.query.sorting,
					kondisi : req.query.kondisi,
					kategori_menu : results[1],
					kategori : results[2][0]

				});
			})
		}else{
			let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; SELECT * FROM tbl_produk WHERE NAMA LIKE '%"+req.query.keyword+"%' ORDER BY "+req.query.sorting+" "+req.query.kondisi+" ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
			let query2 = conn.query(sql2,[3,1], (err, hasil) => {
				console.log(results);
				res.render('kategori',{
					keranjang: hasil[0],
					akun_email: req.ipInfo.ip,
					login : "kosong",
					foto : "picture-1608628151526.png",
					produk : results[0],
					keyword : req.query.keyword,
					sorting : req.query.sorting,
					kondisi : req.query.kondisi,
					kategori_menu : results[1],
					kategori : results[2][0]
				});
			})
		}
	})
});

app.get('/profil_akun', function(req, res) {
	if (req.session.loggedin) {

		var request = require("request");
		var data;
		var options = {
		  method: 'GET',
		  url: 'https://api.rajaongkir.com/starter/city',
		  
		  headers: {key: key_rajaongkir}
		};

		function data_ongkir(callback){
			request(options, function data_ongkir(error, res, body) {
				if (error) throw new Error(error);
				data = JSON.parse(body)
				callback(data.rajaongkir.results);
			});
		}

		data_ongkir(function(data){
			let sql = "SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; ";
			let query = conn.query(sql, [3,1] ,(err, results) => {
				let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
				let query2 = conn.query(sql2,[2,1], (err, hasil) => {

					res.render('profil_akun',{
						akun: results[0],
						keranjang: hasil[0],
						akun_email: req.session.email,
						login : "ada",
						rajaongkir:data,
						kategori_menu : hasil[1]
					});
				})
			})
		})
	}else{
		res.redirect('/');
	}
})

app.post('/daftar', function(req, res) {
	upload_profil(req, res, err => {
       	let sql = "SELECT * FROM tbl_akun WHERE EMAIL = '"+req.body.email+"'";
		let query = conn.query(sql, (err, results) => {
	 		if(results.length == 0){
	 			var sql2 = "";
	 			if (req.file != undefined) {
					sql2 = "INSERT INTO tbl_akun (EMAIL,PASSWORD,NAMA,TANGGAL_LAHIR,JENIS_KELAMIN,ALAMAT,KODE_KOTA,TELEPON,FOTO) VALUES ('"+req.body.email+"','"+req.body.password+"','"+req.body.nama+"',STR_TO_DATE('"+req.body.tanggal_lahir+"', '%Y-%m-%d'),'"+req.body.jenis_kelamin+"','"+req.body.alamat+"','"+req.body.kota+"','"+req.body.telp+"','"+req.file.filename+"')";
				}else{
					sql2 = "INSERT INTO tbl_akun (EMAIL,PASSWORD,NAMA,TANGGAL_LAHIR,JENIS_KELAMIN,ALAMAT,KODE_KOTA,TELEPON) VALUES ('"+req.body.email+"','"+req.body.password+"','"+req.body.nama+"',STR_TO_DATE('"+req.body.tanggal_lahir+"', '%Y-%m-%d'),'"+req.body.jenis_kelamin+"','"+req.body.alamat+"','"+req.body.kota+"','"+req.body.telp+"')";
				}
				let query2 = conn.query(sql2, (err, hasil) => {
					if (err) throw err;
					res.render('berhasil_daftar',{});
				});
	 		}else{
	 			res.render('pendaftaran_gagal',{});
	 		}
		})
    });
})

app.post('/update_profil', function(req, res,next) {
	
	let sql = "UPDATE tbl_akun set NAMA = '"+req.body.nama+"',TANGGAL_LAHIR = STR_TO_DATE('"+req.body.tanggal_lahir+"', '%Y-%m-%d') ,JENIS_KELAMIN = '"+req.body.jenis_kelamin+"',ALAMAT = '"+req.body.alamat+"',KODE_KOTA = '"+req.body.kota+"' WHERE EMAIL = '"+req.session.email+"';";
	let query = conn.query(sql, [1,0], (err, results) => {
 		
	})
	
	res.redirect('/profil_akun');
});

app.post('/update_password', function(req, res,next) {
	
	let sql = "UPDATE tbl_akun set PASSWORD = '"+req.body.password_baru+"' WHERE EMAIL = '"+req.session.email+"';";
	let query = conn.query(sql, [1,0], (err, results) => {
 		
	})
	
	res.redirect('/profil_akun');
});

app.post('/update_foto', function(req, res){
   upload_profil(req, res, err => {
       if (err) throw err
       var sql = "update tbl_akun set FOTO = '"+req.file.filename+"' where EMAIL = '"+req.session.email+"'";
       conn.query(sql, function(err, results){
       	res.redirect('/profil_akun');
       })
    });
});

app.get('/detail', function(req, res,next) {


		var request = require("request");
		var data;
		var options = {
		  method: 'GET',
		  url: 'https://api.rajaongkir.com/starter/city',
		  
		  headers: {key: key_rajaongkir}
		};

		function data_ongkir(callback){
			request(options, function data_ongkir(error, res, body) {
				if (error) throw new Error(error);
				data = JSON.parse(body)
				callback(data.rajaongkir.results);
			});
		}

		data_ongkir(function(data){
     		let sql = "select * from tbl_produk where KODE = '"+req.query.kode+"' ; select * from tbl_produk ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
			let query = conn.query(sql, [3,1], (err, results) => {
				if(req.session.loggedin){
					let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"' ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
					let query2 = conn.query(sql2, [3,1], (err, hasil) => {
					//console.log(data[115])
			     		res.render('detail',{
							kode: results[0][0].KODE,
							nama: results[0][0].NAMA,
							harga: results[0][0].HARGA,
							stok: results[0][0].STOK,
							kategori: results[0][0].KATEGORI,
							gambar: results[0][0].GAMBAR,
							deskripsi: results[0][0].DESKRIPSI,
							results: results[1],
							rajaongkir:data,
							keranjang: hasil[0][0],
							isi_keranjang: hasil[1],
							akun_email: req.session.email,
							login: "ada",
							foto:hasil[2][0].FOTO,
							kategori_menu : results[2]
						});
			     	})
				}else{
					let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' ";
					let query2 = conn.query(sql2, [2,1], (err, hasil) => {
					//console.log(data[115])
			     		res.render('detail',{
							kode: results[0][0].KODE,
							nama: results[0][0].NAMA,
							harga: results[0][0].HARGA,
							stok: results[0][0].STOK,
							kategori: results[0][0].KATEGORI,
							gambar: results[0][0].GAMBAR,
							deskripsi: results[0][0].DESKRIPSI,
							results: results[1],
							rajaongkir:data,
							keranjang: hasil[0][0],
							isi_keranjang: hasil[1],
							akun_email: req.ipInfo.ip,
							login: "kosong",
							foto:"picture-1608628151526.png",
							kategori_menu : results[2]
						});
			     	})
				}
				
			})
		})	
});

app.post('/masuk_keranjang', function(req, res,next) {
	for (var i = 0; i < req.body.qty ; i++) {
		let sql = "INSERT INTO tbl_cart (EMAIL, KODE_PRODUK) VALUES ('"+req.session.email+"','"+req.body.kode_produk+"');";
		let query = conn.query(sql, [1,0], (err, results) => {
	 		
		})
	}
	res.redirect('/');
});

app.get('/hapus_item', function(req, res,next) {
	if (req.session.loggedin) {
		let sql = "DELETE from tbl_cart WHERE KODE_PRODUK = '"+req.query.kode_barang+"' AND EMAIL = '"+req.session.email+"';";
		let query = conn.query(sql, [1,0], (err, results) => {
	 		res.redirect('/keranjang');
		})
	}else{
		res.redirect('/');
	}
});

app.get('/upload', function(req, res) {
	res.render('index_awal',{});
});

app.get('/login', function(req, res) {
	if (req.session.loggedin) {
		res.redirect('/');
	}else{
		var request = require("request");
		var data;
		var options = {
		  method: 'GET',
		  url: 'https://api.rajaongkir.com/starter/city',
		  
		  headers: {key: key_rajaongkir}
		};

		function data_ongkir(callback){
			request(options, function data_ongkir(error, res, body) {
				if (error) throw new Error(error);
				data = JSON.parse(body)
				callback(data.rajaongkir.results);
			});
		}

		data_ongkir(function(data){
			let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.ipInfo.ip+"'";
			let query2 = conn.query(sql2, (err, hasil) => {
				res.render('login',{
					rajaongkir:data,
					akun_email: req.ipInfo.ip,
					login : "kosong",
					foto : "picture-1608628151526.png",
					keranjang: hasil[0],
					wrong: req.query.wrong
				});
			})
		})
	}
});

app.post('/proses_login', function(req, res) {
	var email = req.body.email;
	var password = req.body.password;
	if (email && password) {
		conn.query('SELECT * FROM tbl_akun WHERE EMAIL = ? AND PASSWORD = ?', [email, password], function(error, results, fields) {
			if (results.length > 0) {
				console.log(results);
				var string=JSON.stringify(results);
				var json =  JSON.parse(string);
				console.log('>> json: ', json);
				
				req.session.loggedin = true;
				req.session.email = email;

				res.redirect('/');
				
			} else {
				res.redirect('/login?wrong=true');
			}			
			res.end();
		});
	} else {
		res.send('Please enter Username and Password!');
		res.end();
	}
});

app.get('/keranjang', function(req, res) {
	let sql = "select * from tbl_produk ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
	let query = conn.query(sql, [2,1], (err, results) => {
		
			if (req.session.loggedin) {
				let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' group by tbl_cart.KODE_PRODUK ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"'";
				let query2 = conn.query(sql2, [3,1], (err, hasil) => {
					res.render('cart',{
						results: results,
						keranjang: hasil[0][0],
						isi_keranjang: hasil[1],
						akun_email: req.session.email,
						login: "ada",
						foto: hasil[2][0].FOTO,
						kategori_menu : results[1]
					});
				})
				
			}else{
				res.redirect('/');
			}
	})
});

app.get('/riwayat_transaksi', function(req, res) {
	if (req.session.loggedin) {
		let sql = "select * from tbl_produk ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query = conn.query(sql, [2,1],(err, results) => {
			
			let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"' ; select count(*) as JUMLAH, tbl_produk.KODE, tbl_produk.GAMBAR, tbl_produk.NAMA, tbl_produk.HARGA from tbl_produk JOIN tbl_cart ON tbl_produk.KODE = tbl_cart.KODE_PRODUK where tbl_cart.EMAIL = '"+req.session.email+"' group by tbl_cart.KODE_PRODUK ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.EMAIL = '"+req.session.email+"' AND STATUS != '' GROUP BY tbl_transaksi.KODE ORDER BY WAKTU DESC ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.EMAIL = '"+req.session.email+"' AND tbl_transaksi.STATUS = 'UNPAID' GROUP BY tbl_transaksi.KODE ORDER BY WAKTU DESC ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.EMAIL = '"+req.session.email+"' AND tbl_transaksi.STATUS = 'PAID' GROUP BY tbl_transaksi.KODE ORDER BY WAKTU DESC ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.EMAIL = '"+req.session.email+"' AND tbl_transaksi.STATUS = 'ON PROCESS' GROUP BY tbl_transaksi.KODE ORDER BY WAKTU DESC ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.EMAIL = '"+req.session.email+"' AND tbl_transaksi.STATUS = 'SEND' GROUP BY tbl_transaksi.KODE ORDER BY WAKTU DESC ; SELECT tbl_transaksi.KODE, tbl_transaksi.WAKTU, tbl_transaksi.EMAIL, tbl_transaksi.TOTAL, tbl_transaksi.ONGKIR, GROUP_CONCAT(tbl_detail_transaksi.KODE_PRODUK) as KODE_PRODUK, GROUP_CONCAT(tbl_detail_transaksi.JUMLAH) as JUMLAH, GROUP_CONCAT(tbl_produk.NAMA) as NAMA, GROUP_CONCAT(tbl_produk.HARGA) as HARGA, GROUP_CONCAT(tbl_produk.GAMBAR) as GAMBAR, tbl_transaksi.STATUS from tbl_transaksi JOIN tbl_detail_transaksi ON tbl_transaksi.KODE = tbl_detail_transaksi.KODE_TRANSAKSI JOIN tbl_produk ON tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE WHERE tbl_transaksi.EMAIL = '"+req.session.email+"' AND tbl_transaksi.STATUS = 'DONE' GROUP BY tbl_transaksi.KODE ORDER BY WAKTU DESC ; SELECT * FROM tbl_akun WHERE EMAIL = '"+req.session.email+"'";
			let query2 = conn.query(sql2, [9,1], (err, hasil) => {

				res.render('riwayat_transaksi',{
					results: results,
					keranjang: hasil[0][0],
					isi_keranjang: hasil[1],
					akun_email: req.session.email,
					login: "ada",
					daftar_transaksi : hasil[2],
					daftar_transaksi_unpaid : hasil[3],
					daftar_transaksi_paid : hasil[4],
					daftar_transaksi_proses : hasil[5],
					daftar_transaksi_send : hasil[6],
					daftar_transaksi_done : hasil[7],
					foto: hasil[8][0].FOTO,
					kategori_menu : results[1]
				});
			})	
				
		})
	}else{
		res.redirect('/');
	}
});

app.get('/batalkan_pesanan', function(req, res) {
	if (req.session.loggedin) {
		let sql = "select * from tbl_detail_transaksi WHERE KODE_TRANSAKSI = '"+req.query.kode+"' ; SELECT * FROM tbl_transaksi WHERE KODE = '"+req.query.kode+"' ; UPDATE tbl_transaksi set STATUS = 'CANCEL' WHERE KODE = '"+req.query.kode+"'";
		let query = conn.query(sql,[3,1], (err, results) => {
			res.redirect('/riwayat_transaksi');
		})
	}else{
		res.redirect('/');
	}
});

app.get('/konfirmasi_terima', function(req, res) {
	if (req.session.loggedin) {
		let sql = "select * from tbl_detail_transaksi WHERE KODE_TRANSAKSI = '"+req.query.kode+"' ; UPDATE tbl_history set DITERIMA = CURRENT_TIMESTAMP() WHERE KODE = '"+req.query.kode+"' ; UPDATE tbl_transaksi set STATUS = 'DONE' WHERE KODE = '"+req.query.kode+"'";
		let query = conn.query(sql,[3,1], (err, results) => {
			res.redirect('/riwayat_transaksi');
		})
	}else{
		res.redirect('/');
	}
});

app.post('/beli', function(req, res) {
	var total = req.body.qty * req.body.harga_produk;
	let sql = "INSERT INTO tbl_transaksi (EMAIL, TOTAL) VALUES ('"+req.session.email+"','"+total+"');";
	let query = conn.query(sql, [1,0], (err, results) => {
		var kode_transaksi = results.insertId;
		let sql2 = "INSERT INTO tbl_detail_transaksi (KODE_TRANSAKSI, KODE_PRODUK, JUMLAH) VALUES ('"+kode_transaksi+"','"+req.body.kode_produk+"','"+req.body.qty+"'); ";
		let query2 = conn.query(sql2, [1,0], (err, hasil) => {
			res.redirect('/checkout?kode_transaksi='+kode_transaksi);
		})
	});
});

app.post('/bayar_cart', function(req, res) {
	let sql = "INSERT INTO tbl_transaksi (EMAIL, TOTAL) VALUES ('"+req.session.email+"','"+req.body.subtotal+"');";
	let query = conn.query(sql, [1,0], (err, results) => {
		var kode_transaksi = results.insertId;
		var list_produk = req.body.list_produk;
		var list_jumlah = req.body.list_jumlah;
  		
  		var array_produk = list_produk.split(";");
  		var array_jumlah = list_jumlah.split(";");

  		for(var i=0 ; i<array_produk.length ; i++){
  			let sql2 = "INSERT INTO tbl_detail_transaksi (KODE_TRANSAKSI, KODE_PRODUK, JUMLAH) VALUES ('"+kode_transaksi+"','"+array_produk[i]+"','"+array_jumlah[i]+"'); select * from tbl_akun where EMAIL = '"+req.session.email+"' ; select * from tbl_detail_transaksi where KODE_TRANSAKSI = '"+kode_transaksi+"'";
			let query2 = conn.query(sql2, [1,0], (err, hasil) => {})
  		}
  		res.redirect('/checkout?kode_transaksi='+kode_transaksi);
	});
});

app.get('/checkout', function(req, res) {

	var request = require("request");

	var options = {
	  	method: 'GET',
	  	url: 'https://api.rajaongkir.com/starter/city',
	  
	  	headers: {key: key_rajaongkir}
	};

	function data_ongkir(callback){
		request(options, function data_ongkir(error, res, body) {
			if (error) throw new Error(error);
			data = JSON.parse(body)
			callback(data.rajaongkir.results);
		});
	}

  	data_ongkir(function(data){
  		let sql = "select * from tbl_produk ; SELECT KODE, LOWER(NAMA) as NAMA FROM tbl_kategori";
		let query = conn.query(sql,[2,1],(err, results) => {
			
				console.log(req.ipInfo.ip);//get client ip
				
				if (req.session.loggedin) {
					let sql2 = "select count(*) as JUMLAH from tbl_cart where EMAIL = '"+req.session.email+"'";
					let query2 = conn.query(sql2, (err, hasil) => {

						let sql3 = "select * from tbl_akun where EMAIL = '"+req.session.email+"' ; select tbl_produk.KODE, tbl_produk.NAMA, tbl_produk.HARGA, tbl_detail_transaksi.JUMLAH from tbl_detail_transaksi JOIN tbl_produk on tbl_detail_transaksi.KODE_PRODUK = tbl_produk.KODE where KODE_TRANSAKSI = '"+req.query.kode_transaksi+"' ; select * from tbl_transaksi where KODE = '"+req.query.kode_transaksi+"' ";
						let query3 = conn.query(sql3, [3,1], (err, hasil2) => {
							res.render('checkout',{
								akun : hasil2[0][0],
								barang : hasil2[1],
								transaksi : hasil2[2][0],
								rajaongkir:data,
								kode_transaksi:req.query.kode_transaksi,
								keranjang: hasil[0],
								akun_email: req.session.email,
								login : "ada",
								foto: hasil2[0][0].FOTO,
								kategori_menu: results[1]
							});
						})
					})
					
				}else{
					res.redirect('/');
				}
		})

  		
  	})
});


app.get('/logout', function(req, res) {
	if (req.session.loggedin = true) {
		req.session.loggedin = false;
		req.session.email = "-";
		req.session.destroy();
		res.redirect('/');
	} else {
		res.redirect('/');
	}
});

app.get('/logout_admin', function(req, res) {
	if (req.session.loggedin_admin = true) {
		req.session.loggedin_admin = false;
		req.session.email_admin = "-";
		req.session.destroy();
		res.redirect('/admin');
	} else {
		res.redirect('/admin');
	}
});

app.post('/addPicture', function(req, res){
   upload(req, res, err => {
       if (err) throw err
       var sql = "update tbl_produk set GAMBAR = '"+req.file.filename+"', NAMA = '"+req.body.nama+"', HARGA = '"+req.body.harga+"' where KODE = '"+req.body.kode+"'";
       conn.query(sql, function(err, results){
       	res.redirect('/upload');
       })
    });
});

app.get('/obd', function(req, res) {
	res.render('obd',{});
});

app.get('/gps', function(req, res) {
	res.render('map',{});
});

app.get('/history_obd', function(req, res) {
	res.render('history_obd',{});
});

app.post('/delete_history_obd', function(req, res) {	
	conn.query("DELETE from tbl_obd where KODE = '"+req.body.kode+"'", function (err, result, fields) {
		conn.query("SELECT * FROM tbl_obd order by WAKTU desc limit 1", function (err, result, fields) {
	    	if (err) throw err;
	    	let data = JSON.stringify(result);
			fs.writeFileSync('./public/data_obd.json', data);
			//res.redirect('/history');
  		});
	});
});

app.get('/delete_all_obd', function(req, res) {	
	conn.query("DELETE from tbl_obd ", function (err, result, fields) {
		conn.query("SELECT * FROM tbl_obd order by WAKTU desc limit 1", function (err, result, fields) {
	    	if (err) throw err;
	    	let data = JSON.stringify(result);
			fs.writeFileSync('./public/data_obd.json', data);
  		});
	});
});

app.get('/history_gps', function(req, res) {
	res.render('history_gps',{});
});

app.post('/delete_history_gps', function(req, res) {	
	conn.query("DELETE from tbl_gps where KODE = '"+req.body.kode+"'", function (err, result, fields) {
		conn.query("SELECT * FROM tbl_gps order by WAKTU desc limit 1", function (err, result, fields) {
	    	if (err) throw err;
	    	let data = JSON.stringify(result);
			fs.writeFileSync('./public/data_gps.json', data);
  		});
	});
});

app.post('/delete_all_gps', function(req, res) {	
	conn.query("DELETE from tbl_gps", function (err, result, fields) {
		conn.query("SELECT * FROM tbl_gps order by WAKTU desc limit 1", function (err, result, fields) {
	    	if (err) throw err;
	    	let data = JSON.stringify(result);
			fs.writeFileSync('./public/data_gps.json', data);
  		});
	});
});

app.get('/tes_esp', function(req, res) {	
	let sql = "INSERT INTO tes_esp (soil,suhu,ph) VALUES ('"+req.query.soil+"','"+req.query.suhu+"','"+req.query.ph+"');";
	let query = conn.query(sql, [1,0], (err, results) => {
		console.log(req.query.soil);
		console.log(req.query.suhu);
		console.log(req.query.ph);
	});
});

app.get('/cek_resi', function(req, res) {	
	var request = require('request');
	var options = {
	  'method': 'GET',
	  'url': 'https://api.cekresi.pigoora.com/cekResi?key=pigoorafreeservices&resi=JNCL-7127560338',
	  'headers': {
	  }
	};
	

	function data_resi(callback){
		request(options, function (error, response, body) {
		  if (error) throw new Error(error);
		  data = JSON.parse(body)
		  callback(data);
		  //console.log(data.rajaongkir.results[0]);
		});
  	}

  	data_resi(function(data){
  		console.log(data.result.manifest);
  	})
});

io.on('connection', socket => {
	console.log("ada socket.io");

  socket.emit('message', 'You have successfully joined the chat')

  socket.on('message', (msg) => {
    //io.emit('message', msg)
    console.log(msg)
    var request = require("request");

	var options = {
	  method: 'POST',
	  url: 'https://api.rajaongkir.com/starter/cost',
	  headers: {key: key_rajaongkir, 'content-type': 'application/x-www-form-urlencoded'},
	  form: {origin: '22', destination: msg, weight: 1100, courier: 'jne'}
	};

	

	function data_ongkir(callback){
		request(options, function (error, response, body) {
		  if (error) throw new Error(error);
		  data = JSON.parse(body)
		  callback(data.rajaongkir.results[0]);
		  //console.log(data.rajaongkir.results[0]);
		});
  	}

  	data_ongkir(function(data){
  		io.emit('hasil', data)
  		console.log(data.costs[0].service);
  		console.log(data.costs[0].cost[0].value);
  		console.log(data.costs[0].cost[0].etd);
  	})
  })

  socket.on('masuk_keranjang', (qty, kode_produk, email) => {
  	for (var i = 0; i < qty ; i++) {
		let sql = "INSERT INTO tbl_cart (EMAIL, KODE_PRODUK) VALUES ('"+email+"','"+kode_produk+"');";
		let query = conn.query(sql, [1,0], (err, results) => {})
	}

	io.emit('update_keranjang', qty);
  })

  socket.on('save_token', (token,kode_transaksi) => {
	let sql = "UPDATE tbl_transaksi set TOKEN = '"+token+"' WHERE KODE = '"+kode_transaksi+"'; ";
	let query = conn.query(sql, [1,0], (err, results) => {})
  })

  socket.on('bayar', (tes,kode_transaksi,total,nama,telepon,email,alamat,kota_tujuan,list_kode,list_barang,list_qty,list_jumlah,ongkir,service,email_akun) => {
  	let sql = "UPDATE tbl_transaksi set PENERIMA = '"+nama+"',TELEPON = '"+telepon+"',EMAIL_PENERIMA = '"+email+"', ALAMAT = '"+alamat+"',KOTA = '"+kota_tujuan+"',ONGKIR = '"+ongkir+"',SERVICE = '"+service+"',STATUS = 'UNPAID' WHERE KODE = '"+kode_transaksi+"' ;";
	let query = conn.query(sql, [1,0], (err, results) => {
		if (err) {
			throw err;
		}
	})

	let sql2 = "INSERT INTO tbl_history (KODE,DIBUAT) VALUES ('"+kode_transaksi+"',CURRENT_TIMESTAMP());";
	let query2 = conn.query(sql2, [1,0], (err, hasil) => {
		if (err) {
			throw err;
		}
	})

  	var kode = list_kode.split(";");
  	var barang = list_barang.split(";");
  	var qty = list_qty.split(";");
  	var jumlah = list_jumlah.split(";");

  	for(var i = 0; i<kode.length-1; i++){
  		let sql = "DELETE from tbl_cart WHERE KODE_PRODUK = '"+kode[i]+"' AND EMAIL = '"+email_akun+"' ;";
		let query = conn.query(sql, [1,0], (err, results) => {})
  	}

  	const midtransClient = require('midtrans-client');
	// Create Snap API instance
	let snap = new midtransClient.Snap({
        // Set to true if you want Production Environment (accept real transaction).
        isProduction : false, 
        serverKey : 'SB-Mid-server-SW30BYMKE3PJw3-1mP3iyPQP' // server key midtrans
    });

	var obj = {
	   item: []
	};

	for(var i = 0; i<kode.length-1; i++){
		obj.item.push({
            id: kode[i],
	  		price: parseInt(jumlah[i]),
	  		quantity: parseInt(qty[i]),
	  		name: barang[i]
        });
	}

	obj.item.push({
        id: "xxx",
  		price: parseInt(ongkir),
  		quantity: 1,
  		name: "BIAYA PENGIRIMAN"
    });

	var item_details = obj.item;
	console.log(item_details);

	let parameter = {
	    "transaction_details": {
	    	"order_id": kode_transaksi,
	    	"gross_amount": total
	  	},
	  	"credit_card": {
	    	"secure": true
	  	},
	  	item_details,
		"customer_details": {
		    "first_name": nama,
		    "email": email,
		    "phone": telepon,
		    "shipping_address": {
		    	"first_name": nama,
		      	"email": email,
		      	"phone": telepon,
		      	"address": alamat,
		      	"city": kota_tujuan,
		      	"country_code": "IDN"
		    }
	  	}
	};

	snap.createTransaction(parameter)
    .then((transaction)=>{
        // transaction token
        let transactionToken = transaction.token;

        io.emit('bayar_tampil', transactionToken);
        
    })
  })

  socket.on('cek_daftar_ongkir', (kota,berat) => {
    var request = require("request");

	var options = {
	  method: 'POST',
	  url: 'https://api.rajaongkir.com/starter/cost',
	  headers: {key: key_rajaongkir, 'content-type': 'application/x-www-form-urlencoded'},
	  form: {origin: '22', destination: kota, weight: berat, courier: 'jne'}
	};

	
	//console.log("aaaa");
	function data_ongkir(callback){
		request(options, function (error, response, body) {
		  if (error) throw new Error(error);
		  data = JSON.parse(body)
		  callback(data.rajaongkir);
		  //console.log(data.rajaongkir.results[0]);
		  //console.log(data.rajaongkir.destination_details);
		});
  	}

  	data_ongkir(function(data){
  		io.emit('daftar_ongkir_tampil', data.results[0],data.destination_details.type,data.destination_details.city_name)
  		// console.log(data.costs[0].cost[0].value);
  		// console.log(data.costs[0].cost[0].etd);
  	})
  })
})

