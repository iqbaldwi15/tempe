const button = document.getElementById('btn_rajaongkir');
const kota = document.getElementById('kota');
const kode_transaksi = document.getElementById('kode_transaksi').value;


const button_bayar = document.getElementById('pay_button');

const bayar_proses = document.getElementById('bayar');

const socket = io('localhost:3000');

socket.on('daftar_ongkir_tampil', (msg,tipe,kota) => {
	document.getElementById('dummy').click();
	for(var i=0;i<msg.costs.length;i++){
		document.getElementById('label_'+[i]).innerHTML = msg.costs[i].service;
		document.getElementById('text_pengiriman_'+i).value = msg.costs[i].cost[0].value;
		document.getElementById("pengiriman_caption_"+i).innerHTML = msg.costs[i].description+"<br>"+msg.costs[i].cost[0].etd+" Hari<br><b>Rp."+msg.costs[i].cost[0].value+"</b><div id='ceklis_"+i+"' class='ceklis'><i class='fas fa-check-circle'></i></div>";	
		document.getElementById('ceklis_'+i).style.display = "none";
	}

	document.getElementById('kota_tujuan').value = tipe + " " + kota;
	document.getElementById('kota_pilih').value = tipe + " " + kota;

	if (msg.costs.length<3) {
		document.getElementById('pengiriman_3').style.display = "none";
	}else{
		document.getElementById('pengiriman_3').style.display = "block";
	}

})

socket.on('bayar_tampil', (tes) => {
	var transactionToken = document.getElementById('transactionToken');
	transactionToken.value = tes;
	document.getElementById('bayar').click();
})

bayar_proses.addEventListener('click', function() {
	const transactionToken = document.getElementById('transactionToken').value;
	socket.emit('save_token',transactionToken,kode_transaksi);
});

button_bayar.addEventListener('click', function() {
	const total = document.getElementById('total_text').value;
	const nama = document.getElementById('nama').value;
	const telepon = document.getElementById('telp').value;
	const email = document.getElementById('email').value;
	const alamat = document.getElementById('alamat').innerHTML;
	const kota_tujuan = document.getElementById('kota_tujuan').value;
	const service = document.getElementById("service").value;

	const email_akun = document.getElementById('email_akun').value;

	const list_kode = document.getElementById('list_kode').value;
	const list_barang = document.getElementById('list_barang').value;
	const list_qty = document.getElementById('list_qty').value;
	const list_jumlah = document.getElementById('list_jumlah').value;

	const ongkir = document.getElementById('ongkir_text').value;
	const transactionToken = document.getElementById('transactionToken').value;

	console.log(transactionToken);

	socket.emit('bayar',"tes",kode_transaksi,total,nama,telepon,email,alamat,kota_tujuan,list_kode,list_barang,list_qty,list_jumlah,ongkir,service,email_akun,transactionToken);
	//socket.emit('pesan', messageBox.value);
	//messageBox.value = ''
});

button.addEventListener('click', function() {
  socket.emit('cek_daftar_ongkir',kota.value, 1000);
  //socket.emit('pesan', messageBox.value);
  //messageBox.value = ''
});

kota.addEventListener('click', function() {
  socket.emit('cek_daftar_ongkir',kota.value, 1000);
  //socket.emit('pesan', messageBox.value);
  //messageBox.value = ''
});

socket.emit('cek_daftar_ongkir',kota.value, 1000);