const select = document.getElementById('tujuan');
const button = document.getElementById('btn_cek');
const area = document.getElementById('text-area');
const form_keranjang = document.getElementById('form_keranjang');

const qty_produk = document.getElementById('qty_keranjang');
const kode_produk = document.getElementById('kode_produk_keranjang');
const email_keranjang = document.getElementById('email_keranjang');
const btn_keranjang = document.getElementById('btn_masuk_keranjang');
var qty_now = document.getElementById('notif_keranjang');

const socket = io('localhost:3000');

socket.on('hasil', (msg) => {
	for(var i=0;i<msg.costs.length;i++){

		var element_td_service = document.getElementById("service_"+i);
		var element_td_harga = document.getElementById("harga_"+i);
		var element_td_estimasi = document.getElementById("estimasi_"+i);

		element_td_service.innerHTML = msg.costs[i].service;
		element_td_harga.innerHTML = "Rp. " + msg.costs[i].cost[0].value;
		element_td_estimasi.innerHTML = msg.costs[i].cost[0].etd + " Hari";
	}

	if (msg.costs.length<3) {
		var element_td_service = document.getElementById("service_2");
		var element_td_harga = document.getElementById("harga_2");
		var element_td_estimasi = document.getElementById("estimasi_2");

		element_td_service.innerHTML = "";
		element_td_harga.innerHTML = "";
		element_td_estimasi.innerHTML = "";
	}
})

socket.on('update_keranjang', (qty) => {
	qty_now.innerHTML = parseInt(qty_now.innerHTML)+parseInt(qty);
	var notif_keranjang = document.getElementById("notif_keranjang").style.display = "block";
	console.log("berhasil");
	//alert("Berhasil Ditambahkan Ke Keranjang");//Diganti pake modal lebih oke
})

button.addEventListener('click', function() {
  socket.emit('message', select.value);
  //socket.emit('pesan', messageBox.value);
  //messageBox.value = ''
});

btn_keranjang.addEventListener('click', function() {
  socket.emit('masuk_keranjang', qty_keranjang.value , kode_produk.value, email_keranjang.value, qty_now.value);
  //socket.emit('pesan', messageBox.value);
  //messageBox.value = ''
});